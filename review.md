# Introduction

The Linux kernel runs at a higher privilege than user programs, and whilst
user's code is not allowed to access kernel memory it is possible for the
kernel to access user memory. This project is to check that the kernel
accesses user memory only when it should.

This document reviews various methods that current kernels use to achieve 
this via either static analysis (build time) or dynamic checking (run time).

The kernel has specific functions to allow access to user space memory
for a long time. These are included for several reasons (including ensuring
the proper fault codes for bad accesses).  These functions do not protect
against any buffer overruns or other incorrect access features.

For more information, see the kernel's Documentation/security/self-protection.rst

# Kernel to user space access

When user-space makes a system call the kernel is entered to do work for
the user code. This may require accessing user memory from kernel context,
which is done via accessor functions such as copy_to_user(). Whilst it is
possible to use a user-supplied pointer directly, it comes with the following
issues:

- The user code may not supply a valid pointer
- The user code may supply a pointer to memory it is not permitted to access.
- The area of memory may not be currently paged in,

The user-accessors functions provide protection against these by
several mechanisms:

- Verification that the pointer is a valid user range.
- Handle any paging from the access as if it was from the user.
- Handle any exception from the access as a error-return from the copy.

It is also possible that the kernel will use DMA to access the memory,
which will either require using DMA safe buffers, specifically mapped
areas of memory or the relevant memory API to ensure that the user areas
are mapped and have physical memory pages available for the kernel to
use.

Signals are delivered from kernel to user-space, but do not include any
information on the initial delivery; thus they do not have any memory access
restrictions. These are often delivered on the exit to system calls, so
should be delivered in user-space context.

If the kernel needs to do something that requires user-space (for example
on-demand module or firmware loading), then the kernel will spawn a new user
process to do this. This ensure the proper isolation between the kernel and
user code.

The kernel may (at the request of another process) allow user memory to be
modified for debugging purposes. This is done at the request of a debugging
process such as gdb, or from another process requesting trace-points be
added.

There should be no other accesses to user space from the kernel and any
occurrences are bugs, such as de-referencing a bad pointer.

# Static analysis

The kernel supports static analysis tools in the build system to find
code problems. To ensure that user-space-provided pointers are not
used in the wrong way, the kernel adds a special tag to those pointers.

See also:
- Static code checking in the kernel: https://elinux.org/images/d/d3/Bargmann.pdf
- Static analysis on the Linux kernel: http://smackerelofopinion.blogspot.com/2017/09/static-analysis-on-linux-kernel.html
- Sparse: a look under the hood: https://lwn.net/Articles/689907/
- Smatch: pluggable static analysis for C: https://lwn.net/Articles/691882/

## Sparse

The sparse tool is a static analysis tool built on a simple C parser
library. The kernel uses features from sparse to look for common issues,
such as the violations of address-space. It is integrated in the kernel
build system and can be run during build (by adding C=<parameter> to the
make) or after build on all the build files.

To support this checking, any function with a user-space pointer as a
parameter requires the `__user` attribute. This is then checked to make
sure that the `__user` attribute is not accidentally removed or added
to a pointer. These pointers are also checked for any attempt to use
the memory referenced directly.

The test code written for this project 
includes a number of examples of the `__user` attribute
pointers being incorrectly used, such as passing to non `__user` functions,
de-referenced and other errors that should be picked up. Note, newer versions
of sparse have their own test-suite so this should not need to be integrated back.

See Documentation/dev-tools/sparse.rst for more information.

Support for sparse was introduced in early 3.x kernel.

### Kernel support

The `__user` attribute is defined in include/linux/compiler_types.h
```
#define __user         __attribute__((noderef, address_space(1)))
#define __kernel       __attribute__((address_space(0)))
```

### Example

using make with C=2 all the build files will be checked. The C=1 version
will check just the files being built during the given invocation of make.

make C=2 drivers/misc/uaccess-test/

```
$ make C=2 drivers/misc/uaccess-test/
 CHECK   drivers/misc/uaccess-test/sparse-test.c
  drivers/misc/uaccess-test/sparse-test.c:33:28: warning: incorrect type in argument 1 (different address spaces)
  drivers/misc/uaccess-test/sparse-test.c:33:28:    expected void [noderef] <asn:1>*to
  drivers/misc/uaccess-test/sparse-test.c:33:28:    got char *data
  drivers/misc/uaccess-test/sparse-test.c:48:36: warning: incorrect type in argument 2 (different address spaces)
  drivers/misc/uaccess-test/sparse-test.c:48:36:    expected void const [noderef] <asn:1>*from
  drivers/misc/uaccess-test/sparse-test.c:48:36:    got char const *data
  drivers/misc/uaccess-test/sparse-test.c:61:19: warning: incorrect type in initializer (incompatible argument 2 (different address spaces))
  drivers/misc/uaccess-test/sparse-test.c:61:19:    expected int ( *read )( ... )
  drivers/misc/uaccess-test/sparse-test.c:61:19:    got int ( *<noident> )( ... )
  drivers/misc/uaccess-test/sparse-test.c:62:19: warning: incorrect type in initializer (incompatible argument 2 (different address spaces))
  drivers/misc/uaccess-test/sparse-test.c:62:19:    expected int ( *write )( ... )
  drivers/misc/uaccess-test/sparse-test.c:62:19:    got int ( *<noident> )( ... )
```

In this output, the address space is denoted in the `<asn:''x''>` as shown in
the above `__attribute__((address_space(''x'')))`. The default address space is
address_space(0) which is not printed by sparse in the output warnings.

### Notes

All examples in this document are with sparse v0.5.2 built from source.

# Approach

## Building a test VM

The test system is a KVM-based virtual machine using the Debian operating
system for the OS. This will allow for testing the features described,
testing the driver code and verifying the features work under VMs.

Note, the process could not be fully automated so there is no script in the
current release to reproduce this.

### qemu

Initial testing on qemu shows that the VM must support passing through
the necessary features. Support for passing through the SMAP/SMEP features
was added in the following series (Sep 2012):
https://lists.gnu.org/archive/html/qemu-devel/2012-09/msg04622.html

## Build test systems

To allow consistent benchmarking, a minimal real system will be built to
run tests. Debian is used as an example Linux OS.

## X86

There are several features in modern x86 CPUs to aide isolation of
user space code from kernel access. These are SMEP (execution) and
SMAP (access). 

### SMEP

This is a feature that prohibits the kernel from executing any code that
is in user memory. The default is to enable SMEP if the CPU feature is
available (unless otherwise requested on the command line by passing
the nosmep to the kernel). The code in arch/x86/kernel/cpu/common.c to
do the relevant setup.

To determine if the feature is enabled in the CPU use the following command: 
```
cat /proc/cpuinfo | grep smep
```

See SMAP about verification of the feature.

Further reading:

- Bypass SMEP: https://www.abatchy.com/2018/01/kernel-exploitation-4

### SMAP

SMAP is an X86 feature that allows controlled access to less priveledged memory
from kernel context, and supported by most Intel and AMD CPU cores.

The kernel since 3.8 has had SMAP which is configured via the
CONFIG_X86_SMAP option which defaults to 'y' (this is only available
to change if CONFIG_EXPERT=y is set). There is also a kernel parameter
called nosmap which can be passed at boot-time.

With virtual machines it should be possible for KVM to pass
this through to the guest instance. 
See https://www.spinics.net/lists/kvm/msg101389.html
KVM MMU documentation in Documentation/virtual/kvm/mmu.txt

Note, for testing we patch the kernel to allow CONFIG_X86_SMAP
to be defined without selecting CONFIG_EXPERT to keep the
resulting configs closer to x86_64_defconfig.

LWN article aobut the initial series: https://lwn.net/Articles/517251/
Wiki: https://en.wikipedia.org/wiki/Supervisor_Mode_Access_Prevention

To determine if the feature is available on the running CPU do:
```
cat /proc/cpuinfo | grep smap
```
and if this is set and the config is enabled then the kernel is
using this. There is no current way to directly read the status
of the SMAP support in the kernel as it does not export the CR4
value.  Part of our debug set allows the reading of CR4 via the
debugfs system via the ```cpu_cr4``` file (bit 21 to enable) and
the ```cpu_cr4_smap``` which is one if this bit is set.

From the review of the current support, it does not defend against
any deliberate miss-use of the current kernel APIs to access user
space code, or bypassing it with DMA or other peripherals.

Possible further work:
- Warn the user if CONFIG_X86_SMAP=y and there is no CPU support
- Can the hypervisor be blocked from similar user accesses?
- How to defend against deliberate miss-use of the API

#### SMAP usage in the kernel

The code in arch/x86/include/asm/smap.h and uses the STAC and CLAC
instructions to enable and disable the feature. STAC disables the
protection and CLAC puts it back on. The feature is globally
enabled by CPU's CR4 control register via bit X86_CR4_SMAP (
initialised in arch/x86/kernel/cpu/common.c code) being set.

The code in arch/x86/include/asm/smap.h provides two ways of
using the STAC and CLAC calls. For C code, the two functions
are provided of clac() and stac(). For ASM there are defines
for ASM_STAC and ASM_CLAC. These allow the features to be
compiled out completely if CONFIG_X86_SMAP is not set and
when it is set, allow the <asm/alternative.h> file to define
run-time removal.

The alternative code creates run-time sections that identify
where code replacements dependant on run-time readable CPU
features, which are applied to the code early in the kernel init.

The following users of the ASM macros are:

| file | notes |
|------|-------|
| arch/x86/entry/entry_32.S | system-call and fault handlers |
| arch/x86/entry/entry_64.S | system-call and fault handlers |
| arch/x86/entry/entry_64_compat.S | system-call and fault handlers |
| arch/x86/include/asm/fpu/internal.h | x86 fpu code may need to fix-up user instructions | 
| arch/x86/include/asm/futex.h | futexes operate on user supplied pointers |
| arch/x86/include/asm/smap.h | this is where the macros and code is defined |
| arch/x86/lib/copy_user_64.S | code to copy to/from user-space |
| arch/x86/lib/getuser.S | code to copy from user-space |
| arch/x86/lib/putuser.S | code to copy to user-space |
| arch/x86/lib/usercopy_32.c | code to copy to/from user-space |

The usage of these macros is limited to the code that either deals with the kernel
entries from user-space or the code that does the kernel-to-user data copies.

These were found with the following git grep.
```
git grep -E 'ASM_STAC|ASM_CLAC' | grep -v STACK | cut -f 1 -d ':' | uniq
```

The following users of the clac() and stac() functions are:

| file | notes |
|------|-------|
| arch/x86/include/asm/checksum_32.h | versions of csum code that deals with user data |
| arch/x86/include/asm/fpu/internal.h | x86 fpu code may need to fix-up user instructions | 
| arch/x86/include/asm/smap.h | this is where the macros and code is defined |
| arch/x86/include/asm/uaccess.h | this header defines user space access functions like copy_to_user() |
| arch/x86/include/asm/xen/hypercall.h | hypervisor code |
| arch/x86/lib/csum-wrappers_64.c | versions of csum code that deals with user data |
| arch/x86/lib/usercopy_64.c | code to copy to/from user-space |

The usage of the clac() and stac() calls is limited to within the
X86 architecture specific 

These were found with the following git grep.
```
git grep -E 'stac\.*\(|clac\.*\(' |  cut -f 1 -d ':' | uniq
```

Due to the way the kernel's alternative code systems works it is not easy to
grep '.o' file disassembly as the clac and stac instructions are placed into
their own. These sections require their respective relocations applied before
they can be read, which is beyond the scope of a simple script.

### Hardware support.

SMAP and SMEP support should be in any Haswell or later Intel
micro-architectures. The table below shows some of the systems
we have tested and whether they have the feature.

| Intel CPU | Family, Model | Has SMAP | notes |
|-----------|---------------|----------|-------|
| Core(TM) i5-2520M   | 6, 53 | no ||
| Core(TM) i5-3320M   | 6, 58 | no | ivybridge, has smep |
| Core(TM) i7-5600U   | 6, 61 | yes ||
| Xeon(R) CPU E3-1275 | 6, 94 | dyes ||
| Core(TM) i7-6700HQ  | 6, 94 | yes | skylake|
| Core(TM) i7-4820K   | 6, 62 | no | ivy bridge |
| Pentium(R) CPU G3220 | 6, 60 | no | haswell |

The processor flags are read from the system features call to determine
if SMEP and SMAP are available. It is possible that not only the processor
type but also the system BIOS may also be important to whether these features
are available. Since the kernel can check this and can modify the run-time
behaviour the role the BIOS and boot has not been investigated further.

### Configuration

The CONFIG_EXPERT is defined under "General setup".
The CONFIG_X86_SMAP is defined in "Processor type and features" as "Supervisor Mode Access Prevention"

## ARM32

The CONFIG_CPU_SW_DOMAIN_PAN option can be set (as long as the config
for LPAE is not set), using ARM hardware MMU domains to ensure that
the kernel cannot access user-space addresses. This feature has been in
the ARM architecture specification since ARMv4.

The CONFIG_CPU_SW_DOMAIN_PAN option was added in Aug 2015.

Note, the user-access functions are in arch/arm/include/asm/uaccess.h

### PAN code

The arch/arm/include/asm/assembler.h defines uaccess_disable, uaccess_enable.
uaccess_save and uaccess_restore macros for use in assembly to change or
save the DACR (Domain Access Control Register) value.

| file | notes |
|------|-------|
| arch/arm/include/asm/assembler.h | where the macros are defined |
| arch/arm/kernel/entry-armv.S | kernel entry points |
| arch/arm/kernel/entry-common.S | kernel entry points |
| arch/arm/mm/abort-ev4.S | kernel entry point for abort handler |
| arch/arm/mm/abort-ev5t.S | kernel entry point for abort handler |
| arch/arm/mm/abort-ev5tj.S | kernel entry point for abort handler |
| arch/arm/mm/abort-ev6.S | kernel entry point for abort handler |
| arch/arm/mm/abort-ev7.S | kernel entry point for abort handler |
| arch/arm/mm/abort-lv4t.S | kernel entry point for abort handler |
| arch/arm/mm/abort-macro.S | kernel entry point for abort handler |
| arch/arm/nwfpe/entry.S | disable user-space on fp instruction handling abort |
| arch/arm/xen/hypercall.S | disable access for hypervisor calls |

Found with:
```
git grep -E "uaccess_save|uaccess_restore|uaccess_disable|uaccess_restore" arch/arm | grep -v uaccess_save_and_enable | grep -v uaccess_restore\(
```

The arch/arm/include/asm/uaccess.h the uaccess_save_and_enable() and
uaccess_restore() are used for controlling the domain value for the calls
that need to access user space.

| file | notes |
|------|-------|
| arch/arm/include/asm/futex.h | futex operates on user-space pointers |
| arch/arm/include/asm/uaccess.h | defines functions and providers user-space access functions |
| arch/arm/kernel/smp_tlb.c | required for certain tlb flush events |
| arch/arm/kernel/swp_emulate.c | Emulated SWP may be in user-space context |
| arch/arm/lib/uaccess_with_memcpy.c | functions to copy to/from user-space |
| arch/arm/mm/alignment.c | Alignment fault may be from user-space |


```
git grep -E "uaccess_save.*\(|uaccess_restore.*\("
```

# Test code

The ```kmod``` directory contains source for various tests. These
tests range from showing that the static analysis code works and
for run time testing. The run time testing code contains code to
do performance testing and code to do robustness testing at run time.

## Test systems

The Intel test system used is an Intel Core i5-7400 (3.0GHz) system
with 8GB RAM installed. A minimal Debian/buster operating system was
installed. (https://www.ebuyer.com/835370-punch-technology-i5-ubuntu-desktop-ldt-703-1015)

The Arm testing used an Raspberry Pi 3 Model B with Raspbian 9.4 installed
on an 16GiB MMC card.

## Performance testing

The ```tests``` directory contains a tool (ultests) which opens a file,
times a number of read or writes to it and then prints the time taken
and stats about the run. This can be used with the test module to run
tests of a simple kernel file read or write.

It is also possible to use ```dd``` with the kernel module to get similar
results.

## Static testing

The ```sparse-test.c``` file has various tests to demonstrate that
sparse can and does show typical issues by default when run over the
source code.

The parent directory has a makefile which includes a "make check"
which runs sparse in the uaccess-test directory and records the
output. The ```sparse-test.c``` file includes markers for any lines
which should trigger an output and then runs an awk script called
```compare-sparse.awk``` to check this.

### Result

The detection shows that sparse picks up all the issues we expect
except for one. There is an issue with passing __user pointers in
to variadic functions like printk(). As of this time we are working
on adding proper printf formatting support.

| build | output |
|-------|--------|
| X86 SMAP | [report](reports/report.kernel-smap.md) |
| X86 No SMAP | [report](reports/report.kernel-nosmap.md) |
| ARM32 PAN | [report](reports/report.kernel-pan.md) |
| ARM32 No PAN | [report](reports/report.kernel-nopan.md) |

The code coverage counts the number of C files checked by sparse for that
build against the objects built. It does not take into account all the C
files in the kernel as some are for other architectures or build configurations.

There are some false positives as noted below. These are down to
the kernel needing to use it's own APIs to get work done, and taking
the appropriate cautions to allow it do so:

| file             | function        | notes            |
|------------------|-----------------|------------------|
| init/do_mounts.c | do_mount_root() | calls ksys_mount() |
| init/do_mounts.c | do_mount_root() | calks ksys_chdir() |
| init/do_mounts.c | prepare_namespace() | calls ksys_mount() and ksys_chroot() |
| init/do_mounts_initrd.c | handle_initrd() | calls ksys_mount(), ksys_chdir() and ksys_chroot() |
| init/do_mounts_initrd.c | initrd_load() | calls ksys_unlink() 
| init/do_mounts.h | create_dev() | calls ksys_unlink() and ksys_mknod() |
| init/do_mounts.h | bstat() | calls vfs_stat() |
| init/initramfs.c | xwrite() | calls ksys_write() |
| init/initramfs.c | clean_path() | calls vfs_lstat(), ksys_rmdir() and ksys_unlink() |
| init/initramfs.c | do_name() | calls a number of ksys_ based functions |
| drivers/base/devtmpfs.c | devtmpfs_mount() | calls ksys_mount() |
| drivers/base/devtmpfs.c | devtmpfsd() | calls a number of ksys_ based functions |
| fs/exec.c | get_user_arg_ptr() | PTR_ERR() being cast to __user. 

These are reports that may fixing:

- In fs/exec.c, do_execve_file() casts void * to user-pointer. It is not easy code to follow to see if it needs fixing.
- In fs/compat_ioctl.c the mt_ioctl_trans() cats __user to an non-user void. Not sure if this is wrong or how to fix.
- In serial_struct_ioctl() it is possible there is a missing __user here, but pointer is an iomem. Difficult to fix.
- In fs/fcntl.c fcntl_rw_hint() casts to __user but then assigns to a non __user variable. This looks to be wrong.
- In net/core/scm.c possible issue with CMSG_DATA() macro not returning __user annotated pointer. The best way would be to fix the CMSG_DATA() to pass through the address-space, or fix spare to work out there's no address-space change.
- In arch/x86/lib/usercopy_64.c copy_user_handle_tail() should take __user but does not (will send fix upstream). This is called from assembly code.
- In arch/x86/kernel/crash_dump_64.c the copy_oldmem_page() call can operate either on user-space or kernel space code.
- In net/core/filter.c, bpf_prog_create_from_user calls  on non-user pointer (but not easy to fix due to structure being used for other things).
- In drivers/gpu/drm/drm_ioc32.c there are some casting issues that cannot be easily fixed.

The kernel/trace/trace_kprobe.c FETCH_FUNC_NAME() uses this but the code
is not easy to trace where this is coming from. This has not been verified
if it is a false positive yet.

In net/ipv6/ipv6_sockglue.c do_ipv6_getsockopt() the msghdr is not __user
annotated, but changing this may have wider effects.

In net/compat.c has a few issues with __user not being there, but it is not
clear how best to fix these 

## Coverage

The static testing only covers the configurations being built so there
are parts of the kernel that are not being built in the architectures,
drivers and the necessary support code not being built. 

The sparse tool only checks C files, which means there are objects built
from assembly source that are it cannot check. These files can only
currently be checked. For x86, there are 162 .S files compared to 2520 C
files built for the default configuration.

## Dynamic testing

The kmod code includes a number of tests that work via various
kernel interfaces to allow both performance and robustness to be
tested.

There are some parts of the code that require kernel patches to
work. See the kmod directory readme.md for more details of what
this driver provides.

These tests only currently cover a subset of the user to kernel interfaces
(file read and write) to try to get an idea if the copy to and from user
interfaces are working and that protection mechanisms cope with a basic
set of expected operations. These tests could be extended with:

- Check other parts of the user to kernel APIs, such as ioctl()
- Check the hypervisor use cases
- Boundary accesses (page edge, unaligned access)
- Different access sequences

### SMAP: X86 direct access

To show the run-time behaviour of SMAP on an X86 machine (QEMU) the following
command was used with the test module to issue a direct read from user-space
on a kernel with SMAP enabled.

```
dd if=/sys/kernel/debug/uat/uat-direct of=/dev/null bs=1 count=1
```

Result
```
[  262.293828] BUG: unable to handle kernel paging request at 000055dd90162000
[  262.295176] PGD 800000001c14b067 P4D 800000001c14b067 PUD 1eac8067 PMD 1d5c40
[  262.297089] Oops: 0002 [#1] SMP PTI
[  262.297766] CPU: 0 PID: 1769 Comm: dd Not tainted 4.18.0-smap+ #7
[  262.299221] Hardware name: QEMU Standard PC (i440FX + PIIX, 1996), BIOS 1.124
[  262.300901] RIP: 0010:memcpy_erms+0x6/0x10
[  262.301731] Code: 90 90 90 90 eb 1e 0f 1f 00 48 89 f8 48 89 d1 48 c1 e9 03 8 
[  262.305448] RSP: 0018:ffffaacb00183e08 EFLAGS: 00010216
[  262.309505] RAX: 000055dd90162000 RBX: 000000000000000a RCX: 000000000000000a
[  262.310508] RDX: 000000000000000a RSI: ffff8bac5d67c00c RDI: 000055dd90162000
[  262.312005] RBP: ffff8bac5ddc6a00 R08: 0000000000000000 R09: 0000000000000000
[  262.314989] R10: ffffaacb00183ed8 R11: 0000000000000000 R12: ffff8bac5d9196c0
[  262.317235] R13: 000055dd90162000 R14: 000000000000000a R15: ffffaacb00183f08
[  262.318489] FS:  00007f4c9aa52540(0000) GS:ffff8bac5fc00000(0000) knlGS:00000
[  262.319917] CS:  0010 DS: 0000 ES: 0000 CR0: 0000000080050033
[  262.321124] CR2: 000055dd90162000 CR3: 000000001d770002 CR4: 0000000000360ef0
[  262.322217] DR0: 0000000000000000 DR1: 0000000000000000 DR2: 0000000000000000
[  262.323524] DR3: 0000000000000000 DR6: 00000000fffe0ff0 DR7: 0000000000000400
[  262.324590] Call Trace:
[  262.325053]  uat_area_dread+0x20/0x30
[  262.325875]  full_proxy_read+0x4e/0x70
[  262.326677]  __vfs_read+0x31/0x160
[  262.327454]  vfs_read+0x85/0x130
[  262.328153]  ksys_read+0x4a/0xb0
[  262.328989]  do_syscall_64+0x43/0xf0
[  262.329712]  entry_SYSCALL_64_after_hwframe+0x44/0xa9
[  262.330808] RIP: 0033:0x7f4c9a97d1d1
[  262.331585] Code: fe ff ff 48 8d 3d 07 c2 09 00 48 83 ec 08 e8 06 03 02 00 6 
[  262.334873] RSP: 002b:00007fff54fdddb8 EFLAGS: 00000246 ORIG_RAX: 00000000000
[  262.335762] RAX: ffffffffffffffda RBX: 000055dd8f0d93e0 RCX: 00007f4c9a97d1d1
[  262.336965] RDX: 000000000000000a RSI: 000055dd90162000 RDI: 0000000000000000
[  262.338180] RBP: 000000000000000a R08: 0000000000000003 R09: 00007fff54ff6080
[  262.339458] R10: 000000000000039c R11: 0000000000000246 R12: 000055dd90162000
[  262.340614] R13: 0000000000000000 R14: 0000000000000000 R15: 00007fff54fde010
[  262.341796] Modules linked in:
[  262.342260] CR2: 000055dd90162000
[  262.342801] ---[ end trace f904268a6a005c80 ]---
[  262.343398] RIP: 0010:memcpy_erms+0x6/0x10
[  262.343921] Code: 90 90 90 90 eb 1e 0f 1f 00 48 89 f8 48 89 d1 48 c1 e9 03 8 
[  262.346534] RSP: 0018:ffffaacb00183e08 EFLAGS: 00010216
[  262.347249] RAX: 000055dd90162000 RBX: 000000000000000a RCX: 000000000000000a
[  262.348227] RDX: 000000000000000a RSI: ffff8bac5d67c00c RDI: 000055dd90162000
[  262.349196] RBP: ffff8bac5ddc6a00 R08: 0000000000000000 R09: 0000000000000000
[  262.350276] R10: ffffaacb00183ed8 R11: 0000000000000000 R12: ffff8bac5d9196c0
[  262.351221] R13: 000055dd90162000 R14: 000000000000000a R15: ffffaacb00183f08
[  262.352334] FS:  00007f4c9aa52540(0000) GS:ffff8bac5fc00000(0000) knlGS:00000
[  262.353397] CS:  0010 DS: 0000 ES: 0000 CR0: 0000000080050033
[  262.354255] CR2: 000055dd90162000 CR3: 000000001d770002 CR4: 0000000000360ef0
[  262.355180] DR0: 0000000000000000 DR1: 0000000000000000 DR2: 0000000000000000
[  262.356168] DR3: 0000000000000000 DR6: 00000000fffe0ff0 DR7: 0000000000000400
Killed
```

And the same again with write

```
dd if=/dev/zero of=/sys/kernel/debug/uat/uat-direct bs=1 count=1

```

Result
```
[  825.587153] BUG: unable to handle kernel paging request at 000055f92773b000
[  825.588375] PGD 800000001e982067 P4D 800000001e982067 PUD 1e988067 PMD 1e3d87
[  825.589904] Oops: 0001 [#2] SMP PTI
[  825.590403] CPU: 0 PID: 1775 Comm: dd Tainted: G      D           4.18.0-sma7
[  825.591501] Hardware name: QEMU Standard PC (i440FX + PIIX, 1996), BIOS 1.124
[  825.592679] RIP: 0010:memcpy_erms+0x6/0x10
[  825.593354] Code: 90 90 90 90 eb 1e 0f 1f 00 48 89 f8 48 89 d1 48 c1 e9 03 8 
[  825.596289] RSP: 0018:ffffaacb0033fe08 EFLAGS: 00010286
[  825.597197] RAX: ffff8bac5d67c00c RBX: 0000000000000001 RCX: 0000000000000001
[  825.598499] RDX: 0000000000000001 RSI: 000055f92773b000 RDI: ffff8bac5d67c00c
[  825.599940] RBP: ffff8bac5d6bdb00 R08: 00007ffffffff000 R09: 0000000000000000
[  825.601425] R10: 0000000000000000 R11: 0000000000000000 R12: ffff8bac5d9196c0
[  825.602945] R13: 000055f92773b000 R14: 0000000000000001 R15: ffffaacb0033ff08
[  825.604598] FS:  00007f07e2237540(0000) GS:ffff8bac5fc00000(0000) knlGS:00000
[  825.606057] CS:  0010 DS: 0000 ES: 0000 CR0: 0000000080050033
[  825.607080] CR2: 000055f92773b000 CR3: 000000001d770005 CR4: 0000000000360ef0
[  825.608204] DR0: 0000000000000000 DR1: 0000000000000000 DR2: 0000000000000000
[  825.609561] DR3: 0000000000000000 DR6: 00000000fffe0ff0 DR7: 0000000000000400
[  825.611441] Call Trace:
[  825.611959]  uat_area_dwrite+0x1d/0x30
[  825.613299]  full_proxy_write+0x4e/0x70
[  825.614330]  __vfs_write+0x31/0x170
[  825.615202]  ? selinux_file_permission+0xeb/0x130
[  825.616504]  ? security_file_permission+0x25/0xa0
[  825.618060]  vfs_write+0xa0/0x1a0
[  825.618812]  ksys_write+0x4a/0xb0
[  825.619372]  do_syscall_64+0x43/0xf0
[  825.619941]  entry_SYSCALL_64_after_hwframe+0x44/0xa9
[  825.620985] RIP: 0033:0x7f07e21622a4
[  825.621644] Code: 89 02 48 c7 c0 ff ff ff ff c3 66 2e 0f 1f 84 00 00 00 00 0 
[  825.625246] RSP: 002b:00007ffda8ffecf8 EFLAGS: 00000246 ORIG_RAX: 00000000001
[  825.626551] RAX: ffffffffffffffda RBX: 0000000000000000 RCX: 00007f07e21622a4
[  825.628212] RDX: 0000000000000001 RSI: 000055f92773b000 RDI: 0000000000000001
[  825.629924] RBP: 0000000000000001 R08: 0000000000000003 R09: 00007ffda901c080
[  825.631637] R10: 00000000000008c6 R11: 0000000000000246 R12: 000055f92773b000
[  825.633337] R13: 0000000000000000 R14: 0000000000000000 R15: 000055f92773b000
[  825.634647] Modules linked in:
[  825.635372] CR2: 000055f92773b000
[  825.636291] ---[ end trace f904268a6a005c81 ]---
[  825.637438] RIP: 0010:memcpy_erms+0x6/0x10
[  825.638801] Code: 90 90 90 90 eb 1e 0f 1f 00 48 89 f8 48 89 d1 48 c1 e9 03 8 
[  825.642928] RSP: 0018:ffffaacb00183e08 EFLAGS: 00010216
[  825.644116] RAX: 000055dd90162000 RBX: 000000000000000a RCX: 000000000000000a
[  825.645719] RDX: 000000000000000a RSI: ffff8bac5d67c00c RDI: 000055dd90162000
[  825.647596] RBP: ffff8bac5ddc6a00 R08: 0000000000000000 R09: 0000000000000000
[  825.650320] R10: ffffaacb00183ed8 R11: 0000000000000000 R12: ffff8bac5d9196c0
[  825.651482] R13: 000055dd90162000 R14: 000000000000000a R15: ffffaacb00183f08
[  825.652660] FS:  00007f07e2237540(0000) GS:ffff8bac5fc00000(0000) knlGS:00000
[  825.653853] CS:  0010 DS: 0000 ES: 0000 CR0: 0000000080050033
[  825.654766] CR2: 000055f92773b000 CR3: 000000001d770005 CR4: 0000000000360ef0
[  825.656286] DR0: 0000000000000000 DR1: 0000000000000000 DR2: 0000000000000000
[  825.657341] DR3: 0000000000000000 DR6: 00000000fffe0ff0 DR7: 0000000000000400
Killed
```

In both these cases the kernel has detected a paging fault and killed
the process that caused the access (dd). There is no special notification
that this access was due to an SMAP trap in the OOPS. The OOPS code and
the CR4 bit 21 being set should indicate if it was SMAP triggered.

```
[  825.589904] Oops: 0001 [#2] SMP PTI
```

The first numeric field is the error_code and bit 2 should be zero for
a supervisor mode, bit 4 should be clear for a memory access and 

See ```smap_violation()``` code in ```arch/x86/mm/fault.c```

Note that this has not been exhaustively tested.

### ARM

The ARM system will produce a "page domain fault" OOPS and the process
will be terminated. For testing this is changed into a catchable fault
so that the tesitng process is not continually killed. Since this is a
unique fault, no example is included here.


# Conclusions

The testing shows the SMAP and PAN work on both x86 and Arm systems
that support them. The testing covers most of the issues and could
be extended to make it more thorough. These features add between 1%
to 5% the execution time of the test syscalls, which is not huge.

These features do not protect against deliberate accesses to user
memory (the kmod shows it is possible to build copies of the code
that does this for the kernel-user API), buffer overflows when
copying data or that some other methods of accessing (such as DMA)
won't cause any issues.

To completely test the features would require much closer checking
of the source, and running extensive tests on all variations of the
CPUs with many different tests. This may not be completable due to
the complexity.

The static testing tools show at build time any issues where there
is improper use of user-pointers, to enable these to be fixed.



# Further work

## Pointer obfuscation

To further ensure that there are no accidental accesses, pointers
passed from user to kernel could be deliberately encoded or encrypted
to ensure that the kernel cannot accidentally use these. It could
also be extended to ensure that any size information is also passed
through.

This would be extensive work, as it would require any user-space
users of the interface to be changed, and to modify all the bits of
kernel code that use this. It may also add further overhead to the

## Further PAN/SMAP tests

There are some extensions that can be done to the testing, to
extend the testing scenarios and verification. These include:

- Testing all native CPU access types, not just 32-bit words
- Running through all combinations of page offsets and alignments
- Adding a user space task with known data pages to target.

# The Author

Ben holds a BSC in Computer Systems and Software Engineering from
the University of York. He has been contributing to the Linux kernel
since Linux 2.6 focusing on hardware enablement and debugging.

Ben has 20 years experience in C programming and over 20 in Arm 32bit assembly.
