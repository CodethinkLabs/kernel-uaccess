#!/bin/sh

KDIR=/home/terry/work/linux-kernel-main
SPARSE=/usr/bin/sparse
KBUILD=../kernel/output/$1

## make a report of SPARSE errors

echo "# Software versions"
echo "| source | version |"
echo "|--------|---------|"
echo -n "| Linux kernel | " ; (cd $KDIR; git branch | grep "^*" | sed -e 's/* //g'; echo -n " "; git rev-parse HEAD) | tr -d '\r''\n' ; echo " |"
echo -n "| Sparse | " ; ($SPARSE --version) | tr -d '\r''\n' ; echo " |"
echo -n "| GCC | " ; cat $KBUILD/.config | grep "Compiler: " | sed -e 's/# Compiler: //g'  | tr -d '\r''\n' ; echo " |"
echo  ""

awk -f report-addr.awk $KBUILD/check.log.noformat $KBUILD/check.log.format | sed -e s@$KDIR@@g

echo
echo "## Objects built"

bash count.sh $KBUILD 2>/dev/null | sed -e s@$KDIR@@g

echo
echo "## Kernel configuration"

awk -f show-config.awk $KBUILD/.config
