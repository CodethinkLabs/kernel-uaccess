# awk script to check number of address space warnings

BEGIN {
    FS=":"
    fileidx=-1
}

FILENAME != ofn { ofn = FILENAME; fileidx+=1  }

#/different address spaces/ {
#    print FNR " " $1 " " fileidx
#    result[$1,fileidx]+=1
#    seen[$1]=$1
#}

/expected.*<asn:1>/ { seen[$1]=$1; result[$1,fileidx]+=1; } 
/got.*<asn:1>/ { seen[$1]=$1; result[$1,fileidx]+=1; } 

END {
    print "## all files checked "
    print "| file | pre -Wformat | with -Wformat |"
    print "|------|--------------|---------------|"

    for (f in seen) {
	print "| " f " | " result[f,0] " | " result[f,1] " |"
    }
    print "## files that differ"
    print "| file | pre -Wformat | with -Wformat |"
    print "|------|--------------|---------------|"
    for (f in seen) {
	if (result[f,0] != result[f,1]) {
	    if (result[f,0] < 1) {
		result[f,0] = 0;
	    }
	    print "| " f " | " result[f,0] " | " result[f,1] " |"
	}
    }
}
