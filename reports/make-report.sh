#!/bin/sh

make_report()
{
    sh build-report.sh $1 > report.$1.md
	mkdir -p outputs
    mv report.$1.md outputs
}

make_report kernel-nosmap
make_report kernel-smap
make_report kernel-nopan
make_report kernel-pan
