BEGIN {
    print "| Configuration | Value |"
    print "|---------------|-------|"
    FS="="
}

/^CONFIG/ { print "| " $1 " | " $2 " |" }

END {
    print ""
}
