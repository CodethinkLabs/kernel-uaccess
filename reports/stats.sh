#!/bin/sh

stat() {
    KOUTDIR=output/kernel-$1
    echo "Statistics for $KOUTDIR"
    
    ( cd $KOUTDIR ; size vmlinux )
    readelf -Sl $KOUTDIR/vmlinux
}

# make stats on all
stat smap
stat nosmap
