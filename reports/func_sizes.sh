#!/bin/sh

read_sizes()
{
    TYPE=$1
    nm --print-size --radix=d output/kernel-$TYPE/vmlinux.o | cut -d ' ' -f 2,4 > funcs.$TYPE
}

read_sizes smap
read_sizes nosmap

diff -y funcs.smap funcs.nosmap

