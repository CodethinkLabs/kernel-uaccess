export KDIR=/home/terry/work/linux-kernel-main
export SPARSE=/usr/bin/sparse

report: kernel-check
	make -C ./reports

kernel-check:
	make -C ./kernel

.PHONY : clean
clean:
	-rm -rf kernel/output
	-rm -rf reports/outpus
