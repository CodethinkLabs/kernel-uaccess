/* elf-alt.c
 *
 * Tool to try and apply the alternatives on x86
 *
 * Copyright (c) 2019 Codethink Ltd.
 */

#include <err.h>
#include <fcntl.h>
#include <libelf.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* entry from the alternative section */
struct alt_ins {
	int32_t		instr_offset;
	int32_t		repl_offset;
	uint16_t	feature;
	uint8_t		orig_len;
	uint8_t		alt_len;
	uint8_t		pad_len;
} __attribute__((__packed__));

static void dump_data(Elf_Data *d)
{
	unsigned i, p;
	uint8_t *data = d->d_buf;

	for (i = 0; i < d->d_size; data += 16, i += 16) {
		printf("%08x:", i);
		for (p = 0;  p < 16; p++)
			printf(" %02x", data[p]);
		printf("\n");
	}
}

int main(int argc, char **argv)
{
	int fd;
	Elf *e;
	Elf_Kind kind;
	size_t shstrndx;

	if (argc  < 2)
		errx(EXIT_FAILURE, "usage: %s [file]", argv[0]);

	if (elf_version(EV_CURRENT) == EV_NONE)
		errx(EXIT_FAILURE, "libelf init failed %s", elf_errmsg(-1));

	fd = open(argv[1], O_RDWR, 0);
	if (fd < 0)
		errx(EXIT_FAILURE, "open '%s' failed\n", argv[1]);

	e = elf_begin(fd, ELF_C_RDWR, NULL);
	if (e == NULL)
		errx(EXIT_FAILURE, "elf_begin() failed: %s.",
		     elf_errmsg(-1));

	kind = elf_kind(e);
	if (kind != ELF_K_ELF)
		errx(EXIT_FAILURE, "not an elf object\n");

	if (elf_getshdrstrndx(e, &shstrndx) != 0)
		errx(EXIT_FAILURE, "elf_getshdrstrndx() failed: %s.",
		     elf_errmsg(-1));

	{
		Elf_Scn *alt = NULL;	// .altinstructions
		Elf_Scn *rep = NULL;	// .altinstr_replace_rep
		Elf_Scn *alt_reloc = NULL; // .rela.altinstructions
		Elf_Scn *ptr;
		Elf64_Shdr *shdr;
		char *name;

		ptr = NULL;
		while ((ptr = elf_nextscn(e, ptr)) != NULL) {
			shdr = elf64_getshdr(ptr);
			if (!shdr) { //todo//
			}

			name = elf_strptr(e, shstrndx, shdr->sh_name);
			if (!name)
				continue;

			printf("read '%s'\n", name);
			if (strcmp(name, ".altinstructions") == 0)
				alt = ptr;
			else if (strcmp(name, ".altinstr_replacement") == 0)
				rep = ptr;
			else if (strcmp(name, ".rela.altinstructions") == 0)
				alt_reloc = ptr;
		}

		// memaccess.o has 8 replacements
		// X86_FEATURE_SMAP             ( 9*32+20) = 308 (134)
		// entries 5 and 8 are SMAP related
		if (alt && rep) {
			Elf64_Shdr *alt_sec;
			Elf64_Shdr *rep_sec;
			unsigned alt_off;
			struct alt_ins *alt_ptr, *alt_end;
			Elf_Data *alt_d, *rep_d;

			alt_sec = elf64_getshdr(alt);
			rep_sec = elf64_getshdr(rep);
			if (!alt_sec || !rep_sec)
				errx(EXIT_FAILURE, "did not find sections");

			alt_d = elf_getdata(alt, NULL);
			rep_d = elf_getdata(rep, NULL);
			if (!alt_d || !rep_d)
				errx(EXIT_FAILURE, "did not find data\n");

			printf("addresses (%p,%p) %p, %p at %p,%p\n",
			       alt_sec, rep_sec,
			       alt_sec->sh_addr, rep_sec->sh_addr,
			       alt_d, rep_d);

			printf("alt:\n");
			dump_data(alt_d);
			printf("replacement:\n");
			dump_data(rep_d);
			printf("\n");

			// looks like both altinstructions and
			// altinstr_replacement have relocation info
			// we probably have to apply .rela.altinstructions
			// before we can do anything else.
			// .rela.altinstructions points into both the .text
			// and the .altinstr_replacement sections
			alt_ptr = alt_d->d_buf;
			alt_end = alt_d->d_buf + alt_d->d_size;
			for (; alt_ptr < alt_end; alt_ptr++) {
				printf("%p: offs %x, repl %x, cpu %x\n",
				       alt_ptr,
				       (int)alt_ptr->instr_offset,
				       (int)alt_ptr->repl_offset,
				       (unsigned int)alt_ptr->feature);
			}
		}
	}

	elf_update(e, ELF_C_NULL);
	elf_update(e, ELF_C_WRITE);

	elf_end(e);
	close(fd);
	return 0;

}
