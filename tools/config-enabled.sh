#!/bin/sh

find_config() {
    if [ -e /proc/config.gz ]; then
		CFGFILE=`mktemp config`
		zcat /proc/config.gz > $CFGFILE
		eval $1=$CFGFILE
		return 0
    fi

    BOOTCFG=/boot/config-$(uname -r)
    if [ -e $BOOTCFG ]; then
		eval $1=$BOOTCFG
		return 0
    fi

    return 1
}

find_config CFGFILE
if [ $? -ne 0 ]; then
    echo "Cannot find any config for running kernel"
    exit 1
fi

grep $1 $CFGFILE

