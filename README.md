# Layout

Kernel to user-space access test code

This repository contains the information on the kernel to user-space access tests and reports.

## Kernel

The directory kernel contains build scripts and kernel config file for both x86 and arm platform.

The build scripts also runs Sparse analysis.

## Reports

The directory reports contains scripts to generate static analysis reports. The reports go to
the outputs folder within the same directory.

## Dynmaic tests

The directory dynamic-tests contains test-code for various dynamic test-cases. It also includes
a kernel module which need to be loaded for those tests to run.

As running the dynamic tests request deploying the images to a hardware(or virtual machine if you
like), this is not automated at moment.

## Tools

The directory tools contains a few helper program to be used in the tests.
