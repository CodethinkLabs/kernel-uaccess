# Test run

## X86

| Kernel  | Read    | Write   |
|---------|---------|---------|
| NO smap | 248.09s | 256.85s |
| SMAP    | 258.74s | 260.86s |
| Result  | 4.29% slower | 1.5% slower |

The test conditions were open(), read() 8000 bytes, close(); repeat x 1000000, repeat x 100

## Raspberry Pi

./ultests -l 20 -s 8192 -r 50000 -w 50000 /sys/kernel/debug/uat/uat-area results-pan.txt
./ultests -l 20 -s 8192 -r 50000 -w 50000 /sys/kernel/debug/uat/uat-area results-nopan.txt


| Kernel  | Read    | Write   | Kernel |
|---------|---------|---------|--------|
| No PAN | 11.163363s | 10.926356 | Linux ts-rig6 4.18.0-nopan-00004-ge78fab6a1515 #13 SMP Tue Oct 30 19:00:34 GMT 2018 armv7l GNU/Linux |
| PAN | 11.368811 | 10.633542 | Linux ts-rig6 4.18.0-pan-00004-ge78fab6a1515 #15 SMP Tue Oct 30 18:59:48 GMT 2018 armv7l GNU/Linux |
| Result | 1.8% slower | 2.6% slower |


