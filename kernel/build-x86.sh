#!/bin/sh
# build two kernels, based on the defconfig in our repository 
# Ben Dooks <ben.dooks@codethink.co.uk>
# Copyright 2018 Codethink Ltd.

# KDIR is where we find the kenrel source
if [ x"$KDIR" = x"" ]; then
    KDIR=`pwd`/../../linux/
    echo "Default KDIR to $KDIR"
fi

if [ ! -e $KDIR/scripts/config ]; then
    echo "Cannot find config script"
    exit 1
fi

# this is the base we use to write the output to
KOUTDIR=`pwd`/output
mkdir -p $KOUTDIR
J=`cat /proc/cpuinfo | grep processor  | wc -l`

# use the self-built sparse by exporting SPARSE environment
# or use system installed sparse
if [ x"$SPARSE" = x"" ]; then
    if [ -x "$(command -v sparse)" ]; then
        SPARSE=$(command -v sparse)
    else
        echo 'Error: sparse is not installed.' >&2
        exit 1
    fi
fi

# need 9p and virtio to do kvm passthrough of a directory
SET_CFGS="VFIO VIRTIO_PCI NET_9P NET_9P_VIRTIO 9P_FS 9P_FSCACHE 9P_FS_POSIX_ACL 9P_FS_SECURITY VIRTIO_BLK VIRTIO_BLK_SCSI VIRTIO_NET VIRTIO_PCI_LEGACY VFIO_NOIOMMU VFIO_PCI VFIO_MDEV VFIO_MDEV_DEVICE"

OFF_CFGS="VIRTIO_CONSOLE HW_RANDOM_VIRTIO DRM_VIRTIO_GPU VIRTIO_BALLOON VIRTIO_INPUT CRYPTO_DEV_VIRTIO VFIO_PCI_VGA VFIO_PCI_IGD NET_9P_DEBUG"

# function to build a specific type of kernel
build() {
    KTYPE=$1
    KCONF=$2
    KOUT=$KOUTDIR/kernel-$KTYPE

    mkdir -p $KOUT
    cp config-x86 $KOUT/build-config
    ( cd $KDIR ;
      ./scripts/config --file $KOUT/build-config --set-val CONFIG_X86_SMAP $KCONF
      ./scripts/config --file $KOUT/build-config --set-val CONFIG_LOCALVERSION \"-$KTYPE\"

      for cfg in $SET_CFGS; do
	  ./scripts/config --file $KOUT/build-config --set-val CONFIG_$cfg y
      done

      for cfg in $OFF_CFGS; do 
	  ./scripts/config --file $KOUT/build-config --set-val CONFIG_$cfg n
      done

      cp $KOUT/build-config $KOUT/.config
      make O=$KOUT oldconfig
    )

    if [ 1 -eq 1 ]; then
	make -j $J -C $KDIR O=$KOUT bzImage modules
	if [ $? -ne 0 ]; then
	    echo "Build failed" >&2
	    exit 1
	fi

	echo "Running sparse on kernel with format check..."
	make -C $KDIR CHECK=$SPARSE O=$KOUT CF=-Wformat C=2 bzImage modules > $KOUT/check.log.format 2>&1
	if [ $? -ne 0 ]; then
	    echo "failed to run sparse on kernel"
	    exit 1
	fi

	echo "Running sparse on kernel with no format check..."
	make -C $KDIR CHECK=$SPARSE O=$KOUT CF=-Wno-format C=2 bzImage modules > $KOUT/check.log.noformat 2>&1
	if [ $? -ne 0 ]; then
	    echo "failed to run sparse on kernel"
	    exit 1
	fi

	echo "Installing kernel into system"
	if [ 0 -eq 1 ]; then
	    sudo -- make -C $KDIR O=$KOUT modules_install
	    sudo -- cp $KOUT/arch/x86/boot/bzImage /boot/kernel-$KTYPE
	fi
    fi
}

# do our builds
build smap y
build nosmap n

