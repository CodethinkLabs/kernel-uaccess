#!/bin/sh -x
# build two kernels, based on the defconfig in our repository 
# Ben Dooks <ben.dooks@codethink.co.uk>
# Copyright 2018 Codethink Ltd.

# KDIR is where we find the kenrel source
if [ x"$KDIR" = x"" ]; then
    KDIR=`pwd`/../../linux/
    echo "Default KDIR to $KDIR"
fi

# this is the base we use to write the output to
KOUTDIR=`pwd`/output
mkdir -p $KOUTDIR
J=`cat /proc/cpuinfo | grep processor  | wc -l`

# use the self-built sparse by exporting SPARSE environment
# or use system installed sparse
if [ x"$SPARSE" = x"" ]; then
    if [ -x "$(command -v sparse)" ]; then
        SPARSE=$(command -v sparse)
    else
        echo 'Error: sparse is not installed.' >&2
        exit 1
    fi
fi


# function to build a specific type of kernel
build() {
    KTYPE=$1
    KCONF=$2
    KOUT=$KOUTDIR/kernel-$KTYPE
    export ARCH=arm
    export CROSS_COMPILE=arm-linux-gnueabihf-

    mkdir -p $KOUT
    cp config-rpi $KOUT/.config
     ( cd $KDIR ;
      yes "" | make -C $KDIR O=$KOUT oldconfig > /dev/null
      ./scripts/config --file $KOUT/.config --set-val CONFIG_CPU_SW_DOMAIN_PAN $KCONF
      ./scripts/config --file $KOUT/.config --set-val CONFIG_LOCALVERSION \"-$KTYPE\"
    )
    make -j $J -C $KDIR O=$KOUT zImage modules dtbs

    ( cd $KDIR;
	echo "Running sparse on kernel with format check..."
	make -C $KDIR CHECK=$SPARSE O=$KOUT CF=-Wformat C=2 bzImage modules > $KOUT/check.log.format 2>&1
	if [ $? -ne 0 ]; then
	    echo "failed to run sparse on kernel"
	    exit 1
	fi

	echo "Running sparse on kernel with no format check..."
	make -C $KDIR CHECK=$SPARSE O=$KOUT CF=-Wno-format C=2 bzImage modules > $KOUT/check.log.noformat 2>&1
	if [ $? -ne 0 ]; then
	    echo "failed to run sparse on kernel"
	    exit 1
	fi
    )      

}

# do our builds
build pan y
build nopan n

