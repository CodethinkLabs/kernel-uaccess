# Testing kernel module

This kernel module source provides interfaces for various functional
and performance testing features that user-space could not do without
this. It can either be built as a module, or installed into an existing
kernel source directory under '''drivers/misc'''

TODO - write more here

# The /sys/kernel/debug/uat directory

This contains a number of files that provide access to the test
features. Other kernel interfaces can also be used, the decision
to make this is down to ensuring known code to test against.

Note, some of the debug features depend on the CPU architecture
and possible kernel patches.

## uat-area

This file provides a read and write to an area of kernel memory
that is allocated at open time. This area is currently defined
to be 8KiB and is used for performance testing of the block copy
functions that move data to and from user space.

## uat-direct

This is the same as uat-area but uses memcpy to move the data.
It will not work when the kernel to user memory protections are
in force.

## uat-probe

This attempts to read data from the user application when the
file is written to. This will return successfully if there was
no data accessible and error if there was any data that could
be touched.

This currently requires architecture specific support (assembly
code) and on ARM32 requires a patch to the abort handler to
allow the domain abort to run the required error path. The X86
code ensures it fits in with the get_user/put_user fault handlers.


todo:
- make it run through all user pages
- run multiple different access attempts

## uat-u8, uat-u16, uat-u32 and uat-u64

These files provide a read and write interface that moves the
data type named in the file prefix to or from user using the
get_user() and put_user() code.
