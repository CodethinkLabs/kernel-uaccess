/* access-test.
 *
 * Ben Dooks <ben.dooks@codethink.co.uk>
 * Copyright 2018 Codethink Ltx
 */

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define PAGE_SIZE	(4096)

static unsigned int verbose = 0;

struct mem_area;

struct mem_area {
	struct mem_area		*next;
	void			*ptr;
	ssize_t			size;
};


static struct mem_area *prep_mem_area(unsigned int size)
{
	int prot = PROT_READ | PROT_WRITE;
	int visib = MAP_ANONYMOUS | MAP_SHARED;
	struct mem_area *area;
	void *ptr;

	area = calloc(sizeof(struct mem_area), 1);
	if (!area)
		return NULL;

	ptr = mmap(NULL, size, prot, visib, 0, 0);
	if (!ptr) {
		perror("mmap");
		return NULL;
	}

	area->ptr = ptr;
	area->size = size;

	memset(ptr, 0xaa, PAGE_SIZE);
	
	return area;
}

static int run_test(void)
{
	char tmp[] = "test";
	ssize_t ret;
	int fd;

	fd = open("/sys/kernel/debug/uat/uat-probe", O_RDWR);
	if (fd < 0) {
		perror("open");
		return 1;
	}

	ret = write(fd, tmp, sizeof(tmp));
	if (ret != sizeof(tmp)) {
		perror("write");
		return 1;
	}

	close(fd);
	return 0;
}

static int check_areas(struct mem_area *list)
{
	struct mem_area *ptr;
	int fail = 0;
	char pg[PAGE_SIZE];

	memset(pg, 0xaa, sizeof(pg));
	
	for (ptr = list; ptr != NULL; ptr = ptr->next) {
		if (verbose)
			printf("checking: area %p (sz %lu)\n", ptr->ptr, ptr->size);

		if (memcmp(pg, ptr->ptr, PAGE_SIZE) != 0) {
			printf("fail: area %p (sz %lu)\n", ptr->ptr, ptr->size);
			fail++;
		}
	}

	return fail;
}

int main(int argc, char **argv)
{
	struct mem_area *list = NULL;
	struct mem_area *area;
	int i;

	/* prepare all our memory areas */
	
	printf("preparing memory areas...\n");
	for (i = 4; i < PAGE_SIZE; i += 4) {
		area = prep_mem_area(i);
		if (!area) {
			fprintf(stderr, "failed to allocate area for %d\n", i);
			return 1;
		}

		area->next = list;
		list = area;
	}

	printf("running test\n");
	run_test();

	printf("checking memory areas...\n");
	check_areas(list);
	
	return 0;
}
