// Licensed under GPLv2
// Matt Green <matthew.green@codethink.co.uk>
// Copyright 2018 Codethink Ltd

struct TestArguments
{
	const char *device;                /* arg1 & arg2 */
	const char *results;
	int reads;
	int writes;
	int size;
	int loops;
	int verbose;
};

extern int verbose; // supplied by main

// Function to execute a loop - processes on arbitrary number of read / write tests
void tests(struct TestArguments testArguments);

