// Licensed under GPLv2
// Matt Green <matthew.green@codethink.co.uk>
// Copyright 2018 Codethink Ltd

#include <argp.h>
#include <stdlib.h>

#include "tests.h"

// ----------------------------------------------------------------------------
// Argp parser is a standard POSIX parser type to parse the command line
// These global objects are documentation and parser options.

// Documentation --------------------------------------------------------------
const char *argp_program_version = "Uaccess userland test v0.1";
const char *argp_program_bug_address = 0; // This shall normally be an email

// Usage documentation
static const char doc[] = "This program takes user instructions and performs\n"
						  "read and writes on a file.\n"
		                  "The purpose is to simulate user-land access of a\n"
		                  "kernel driver.";

// Arguments and options -----------------------------------------------------
// Arguments documentation for usage
static const char args_doc[] = "TESTFILE RESULTSFILE";

// These structs define the options available on the command line
const struct argp_option readTestOpt = {
		"read",
		'r',
		"numberOfReads",
		0,
		"Number of read()s to execute",
		0
};

const struct argp_option writeTestOpt = {
		"write",
		'w',
		"numberOfWrites",
		0,
		"Number of write()s to execute",
		0
};

const struct argp_option sizeOpt = {
		"size",
		's',
		"sizeOfFile",
		0,
		"Number of bytes to Read / Write",
		0
};

const struct argp_option loopOpt = {
		"loop",
		'l',
		"numberOfLoops",
		0,
		"Number of times to Repeat the tests",
		0
};

const struct argp_option verboseOpt = {
		"verbose",
		'v',
		0,
		0,
		"Include diagnostic information",
		0
};

const struct argp_option endOpt = { 0 };

// Process the arguments and options: part of the parser
error_t parserFunction(int key, char *arg, struct argp_state *state)
{
	struct TestArguments *testArguments = state->input;
	static int parseCount = 0;
	parseCount++;
	error_t returnValue = 0;
	switch (key) {
	case 'r':
		testArguments->reads = atoi(arg);
		if (testArguments->reads == 0) {
			printf("** Error: Invalid argument to -r\n");
			argp_usage (state);
		}
		break;
	case 'w':
		testArguments->writes = atoi(arg);
		if (testArguments->writes == 0) {
			printf("** Error: Invalid argument to -w\n");
			argp_usage (state);
		}
		break;
	case 's':
		testArguments->size = atoi(arg);
		if (testArguments->size == 0) {
			printf("** Error: Invalid argument to -s\n");
			argp_usage (state);
		}
		break;
	case 'l':
		testArguments->loops = atoi(arg);
		if (testArguments->loops == 0) {
			printf("** Error: Invalid argument to -l\n");
			argp_usage (state);
		}
		break;
	case 'v':
		// No argument to v
		testArguments->verbose++;
		break;
	case ARGP_KEY_ARG:
	    if (state->arg_num >= 2) {
	    	argp_usage (state); // exits
	    	printf("** Error: Too many arguments\n");
	    }
	    switch (state->arg_num) {
	    case 0:
	    	testArguments->device = arg;
	    	break;
	    case 1:
	    	testArguments->results = arg;
	    }
	    break;
	case ARGP_KEY_END:
		if (state->arg_num < 2) {
		    argp_usage (state);
		    printf("** Error: Insufficient arguments\n");
		}
		break;
	default:
		returnValue = ARGP_ERR_UNKNOWN;
	}
	return returnValue;
}

void diagnosticArgumentsSummary(const struct TestArguments *testArguments) {
	// Diagnostic summary
	if (testArguments->loops)
		printf("Loop argument: %d\n", testArguments->loops);
	if (testArguments->size)
		printf("Size argument: %d\n", testArguments->size);
	if (testArguments->reads)
		printf("Read argument: %d\n", testArguments->reads);
	if (testArguments->writes)
		printf("Write argument: %d\n", testArguments->writes);
	printf("Device: %s\n", testArguments->device);
	printf("Results Path: %s\n", testArguments->results);
}

int main(int argc, char **argv) {

	// main.c only parses the command line and sets default argument values
	// - all testing and recording is done in test.c

	// Options to the command line
	struct TestArguments testArguments = { 0 };
	struct argp_option options[] = {readTestOpt, writeTestOpt, sizeOpt, loopOpt, verboseOpt, endOpt};
	struct argp parser = { options, parserFunction, args_doc, doc };
	argp_parse (&parser, argc, argv, 0, 0, &testArguments);

	// Print diagnostic summary if requested
	if (testArguments.verbose > 1) {
		diagnosticArgumentsSummary(&testArguments);
	}

	// Set default parameters - in case none were set on command line
	if (testArguments.size == 0)
		testArguments.size = 1024;
	if (testArguments.loops == 0)
		testArguments.loops = 1;
	if (testArguments.reads == 0)
		testArguments.reads = 1000000;
	if (testArguments.writes == 0)
		testArguments.writes = 1000000;

	tests(testArguments);
	return 0;
}
