
// Licensed under GPLv2
// Matt Green <matthew.green@codethink.co.uk>
// Copyright 2018 Codethink Ltd

#include "tests.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>

#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#define TEST_SUCCESS 0
#define TEST_FAILED 1
#define TEST_RESULTS_FAILED 2

#define POLARITY_READ_TEST 0
#define POLARITY_WRITE_TEST 1

#define FILE_IN_NSECS 1
#define FILE_IN_SECS 0

// Used for the statistics of each test
struct StatStruct {
	int numberOfOperations;
	int sizeOfOperations;
	double processTime;
	long totalTime;
	double totalTimeSecs;
	double averageTime;
	double variance;
	double stdDev;
	int error;
};

static struct StatStruct stats(long array[], int numReads, int size, double processTime);
static int openResultsFile(const char *resultsPath);
static int fileResults(const char *resultsPath, struct StatStruct stats, int time_nsecs, const char *header);
struct StatStruct singleTest(int numReads, const char *devicePath, const char *resultsPath, const int size, const int polarity);

static const clockid_t clockType = CLOCK_MONOTONIC;

void tests(struct TestArguments testArguments)
{
	// The tests shall be conducted 'loop' times. Tests will aggregate the results of the individual tests,
	// not the individual read / writes
	struct StatStruct masterReadResults = { 0 };
	struct StatStruct masterWriteResults = { 0 };
	struct StatStruct testResults;
	
	// To record the times taken for the individual tests
	long readTimes[testArguments.loops];
	long writeTimes[testArguments.loops];

	// To record the total time taken by the process in all Read tests
	struct timespec startReadTime;
	struct timespec finishReadTime;
	double totalReadTime;

	// To record the total time taken by the process in all Write tests
	struct timespec startWriteTime;
	struct timespec finishWriteTime;
	double totalWriteTime;

	// WRITE TESTS ---------------------------------------------------------------
	// Record the start time
	if (testArguments.writes) {
		clock_gettime(clockType, &startWriteTime);
		for (int i = 0; i < testArguments.loops; i++) {
			testResults = singleTest(testArguments.writes, testArguments.device,
						  testArguments.results, testArguments.size, POLARITY_WRITE_TEST);
			if (testArguments.verbose)
				fileResults(testArguments.results, testResults, FILE_IN_NSECS,
							"\n Individual Write Test ");
			writeTimes[i] = testResults.totalTime;
		}

		// Calculate Delta & Statistics - then file the results
		clock_gettime(clockType, &finishWriteTime);
		totalWriteTime = (finishWriteTime.tv_sec - startWriteTime.tv_sec) +
						 (finishWriteTime.tv_nsec - startWriteTime.tv_nsec) / 1000000000.0;

		masterWriteResults = stats(writeTimes, testArguments.loops, 1, totalWriteTime);

		fileResults(testArguments.results, masterWriteResults, FILE_IN_SECS,
				"\n----------------------------------------"
				"----------------------------------------\n"
				"***** OVERALL WRITE ");
	}

	// READ TESTS ---------------------------------------------------------------
	// Record the start time
	if (testArguments.reads) {
		clock_gettime(clockType, &startReadTime);
		for (int i = 0; i < testArguments.loops; i++) {
			testResults = singleTest(testArguments.reads, testArguments.device,
					testArguments.results, testArguments.size, POLARITY_READ_TEST);
			if (testArguments.verbose)
				fileResults(testArguments.results, testResults, FILE_IN_NSECS,
							"\n Individual Read Test ");
			readTimes[i] = testResults.totalTime;
		}
		// Calculate Delta & Statistics - then file the results
		clock_gettime(clockType, &finishReadTime);
		totalReadTime = (finishReadTime.tv_sec - startReadTime.tv_sec) +
						(finishReadTime.tv_nsec - startReadTime.tv_nsec) / 1000000000.0;

		masterReadResults = stats(readTimes, testArguments.loops, 1, totalReadTime);

		fileResults(testArguments.results, masterReadResults, FILE_IN_SECS,
				"\n----------------------------------------"
				"----------------------------------------\n"
				"***** OVERALL READ ");
	}
	return;
}

// Routine to { open a file, read from it / write to it, close it } repeatedly - stats / results written to resultsPath
// N.B: The file must exist
// Takes:
//  numfReads  : no of times to repeat the test
//  devicePath : name of device to open i.e. /dev/test0 or similar
//  resultsFile: name of file to output results
// Returns: 0 success - 1 error

struct StatStruct singleTest(int numOps,
							 const char *devicePath,
							 const char *resultsPath,
							 const int size,
							 const int polarity)
{
	int status;
	int fd;

	// Per test
	size_t bytesTransacted = 0;
	char buffer[size];

	if (polarity) {
		memset(&buffer, 'k', size);
	}

	// Store the results
	long testTimes[numOps];

	// For timing
	struct timespec resolution;
	struct timespec testStart;
	struct timespec testFinish;
	struct timespec totalStartTime;
	struct timespec totalFinishTime;

	// Record the start time
	clock_gettime(clockType, &totalStartTime);

	if (polarity) {
		fd = open(devicePath, O_WRONLY);
	} else {
		fd = open(devicePath, O_RDONLY);
	};

	if (fd < 0) {
		printf("Error opening device '%s'\n", devicePath);
		status = TEST_FAILED;
	}

	for (int testNum = 0; testNum < numOps; testNum++) {
		clock_gettime(clockType, &testStart);

		if (fd > 0) {
			if (polarity) {
				bytesTransacted = write(fd, &buffer, size);
			} else {
				bytesTransacted = read(fd, &buffer, size);
			}
			if (bytesTransacted != -1) {
				status = TEST_SUCCESS;
			} else {
				close(fd);
				printf("Error %s device\n", polarity ? "writing to" : "reading from");
				status = TEST_FAILED;
			}
		} else {
			status = TEST_FAILED;
		}
		if (status == TEST_FAILED)
			break;
		clock_gettime(clockType, &testFinish);
		testTimes[testNum] = (testFinish.tv_sec - testStart.tv_sec) * 1000000000
				+ testFinish.tv_nsec - testStart.tv_nsec;
	}
	// Process time may differ slightly from the sum of the individual tests
	clock_gettime(clockType, &totalFinishTime);
	double processTime = (totalFinishTime.tv_sec - totalStartTime.tv_sec) +
			(totalFinishTime.tv_nsec - totalStartTime.tv_nsec) / 1000000000.0;

	struct StatStruct returnStats = { 0 };
	if (status == TEST_SUCCESS) {
		returnStats = stats(testTimes, numOps, size, processTime);
	} else {
		returnStats.error = status;
	}
	close(fd);
	return returnStats;
}

// This function collates all the information for an individual test into a single structure
struct StatStruct stats(long array[],
						int numReads,
						int size,
						double processTime)
{
	// Standard deviation is the sqrt of variance
	// Variance = sum of ((x - xavr)^2) / N - 1
	struct StatStruct stats =  { 0, 0.0, 0.0, 0.0, 0.0 };
	double sumOfSquaredDeviations = 0.0;
	double deviation = 0.0;
	int outcasts = 0;

	// Include test properties
	stats.numberOfOperations = numReads;
	stats.sizeOfOperations = size;
	stats.processTime = processTime;

	// Bounds check on trivial to prevent div 0
	if (numReads < 1) {
		return stats;
	} else if (numReads == 1) {
		stats.totalTime = array[0];
		stats.totalTimeSecs = array[0] / 1000000000.0;
		stats.averageTime = array[0];
		return stats;
	}

	for (int testNum = 0; testNum < numReads; testNum++) {
		stats.totalTime += array[testNum];
	}
	stats.averageTime = ((float)stats.totalTime / (float)numReads);
	stats.totalTimeSecs = (float)stats.totalTime / 1000000000.0;

	for (int testNum = 0; testNum < numReads; testNum++ ) {
		deviation = (double)array[testNum] - stats.averageTime;
		if (deviation < stats.averageTime * 10) {
			sumOfSquaredDeviations += (deviation * deviation);
		} else {
			outcasts++;
		}
	}

	// Calculated statistics
	stats.variance = sumOfSquaredDeviations / (numReads - 1 - outcasts); // correct std dev
	stats.stdDev = sqrt(stats.variance);
	return stats;
}

// Open the results file, prevents previous records from being over-written
static int openResultsFile(const char *resultsPath)
{
	// Opening, closing, reopening the file is fine, but we don't want to
	// over-write existing results from this test run
	static bool fileCreatedThisTestRun = false;
	int fd = 0;
	if (fileCreatedThisTestRun) {
		fd = open(resultsPath, O_RDWR | O_APPEND);
	} else {
		fd = open(resultsPath, O_WRONLY | O_TRUNC | O_CREAT,
				  S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH | S_IRGRP | S_IWGRP);
		fileCreatedThisTestRun = 1;
	}
	return fd;
}

// Printf the results and log then in the results file
static int fileResults(const char *resultsPath,
					   struct StatStruct stats,
					   int time_nsecs,
					   const char *header)
{
	int retVal = TEST_SUCCESS;
	int fd = openResultsFile(resultsPath);
	if (fd < 1) {
		printf("Error opening Results File\n");
		retVal = TEST_RESULTS_FAILED;
	} else {
		char resultsBuffer[2000];
			sprintf(resultsBuffer,
					"%sTEST RESULTS\n----------------------------------------\n"
					"Number of operations: %d Size of Operations: %d\n"
					"Total Time taken (process): %fs\n"
				    "Average operation time: %f%s Standard deviation: %f%s\n"
				    "Tests Total time : %fs\n",
					header ? header : "",
					stats.numberOfOperations, (int)stats.sizeOfOperations, stats.processTime,
					time_nsecs ? stats.averageTime : stats.averageTime / 1000000000.0,
					time_nsecs ? "ns" : "s",
					time_nsecs ? stats.stdDev : stats.stdDev / 1000000000.0,
					time_nsecs ? "ns" : "s",
					stats.totalTimeSecs);

		printf("%s", resultsBuffer);
		int check = write(fd, resultsBuffer, strlen(resultsBuffer));
		if (check == -1) {
			printf("Error writing Results File\n");
			retVal = TEST_RESULTS_FAILED;
		}
		close(fd);
	}
	return retVal;
}








