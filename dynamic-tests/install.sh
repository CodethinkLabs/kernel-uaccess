#!/bin/sh
#
# Install the kmod into drivers/misc on the target kernel
#
# Ben Dooks <ben.dooks@codethink.co.uk>
# Copyright 2018 Codethink Ltd

if [ ! -d $KDIR ]; then
    echo "Kernel $KDIR does not exist" >&2
    exit 1
fi

if [ ! -e $KDIR/drivers/misc/uaccess-test ]; then
    ln -s `pwd`/kmod/uaccess-test $KDIR/drivers/misc/uaccess-test
    echo "obj-y += uaccess-test/" >> $KDIR/drivers/misc/Makefile
fi

echo
    
