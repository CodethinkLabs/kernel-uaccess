/* sparse-test2.c
 *
 * Ben Dooks <ben.dooks@codethink.co.uk>
 * Copyright 2018 (c) Codethink Ltd.
 */

/* an external function sparse/cc shouldn't know anything about */
extern void uat_do_something(const void *ptr, size_t sz);

/* pointer to a function */
extern void (*uat_fn1)(const void *ptr, size_t sz);
