/* sparse-test2.c
 *
 * Ben Dooks <ben.dooks@codethink.co.uk>
 * Copyright 2018 (c) Codethink Ltd.
 */

#include <linux/module.h>
#include <linux/kernel.h>

#include "sparse-test.h"

void uat_do_something(const void *ptr, size_t sz)
{
}

void (*uat_fn1)(const void *ptr, size_t sz) = uat_do_something;

