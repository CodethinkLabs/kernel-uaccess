BEGIN {
}

NR==FNR {
    if ($2 == "warning:") {
	split($1, info, ":");
	if (debug) {
	    print "File is " info[1]
	    print "Line is " info[2]
	    print "Char is " info[3]
	}

	last=""
	for (i = 3; i <= NF; i++) { last = last " " $i }
	warn[info[2]] = last;
    }
    next
}

/* bad */ {
    if (FNR != NR) {
	tagged[FNR] = 1;
	if (debug) { print "tagging expected error " FNR }
    }
}

END {
    printf "INFO: got %d warnings and %d tags in code\n", length(warn), length(tagged)
    for (i in warn) {
	if (tagged[i] == 1) {
	    print "warning on line " i " was tagged in file"
	} else {
	    print "ERROR: warning on line " i " was not tagged! " warn[i]
	}
    }

    for (t in tagged) {
	if (t in warn) {
	    print "warning tag was on line " t " flagged"
	} else {
	    print "ERROR: warning tag on line " t " not found!"
	}   
    }
}
