/* sparse-test.c
 *
 * Ben Dooks <ben.dooks@codethink.co.uk>
 * Copyright 2018 (c) Codethink Ltd.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/debugfs.h>
#include <linux/cdev.h>

#include "sparse-test.h"

#define DEV_NAME "uat"
#define CLASS_NAME "uat"

#define MAX_DEVS 64

static struct dentry *debug_root;

/* All lines with deliberate __user errors are marked with bad */

/* This should warn on the read and write missing the user attribute */

/* the area code can take up to 512 bytes */
static ssize_t uat_sparse_area_read(struct file *f, char  *data,
				    size_t sz, loff_t *off)
{
	char buff[512];
	int ret;

	if (sz > sizeof(buff))
		return -EMSGSIZE;

	ret = copy_to_user(data, buff, sz);	/* bad */
	if (ret)
		return -EFAULT;
	return sz;
}

static ssize_t uat_sparse_area_write(struct file *f, const char *data,
				     size_t sz, loff_t *off)
{
	char buff[512];
	int ret;

	if (sz > sizeof(buff))
		return -EMSGSIZE;

	ret = copy_from_user(buff, data, sz);		/* bad */
	return ret ? -EFAULT : sz;
}

static int uat_sparse_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static const struct file_operations uat_sparse_fops = {
	.owner	= THIS_MODULE,
	.open	= uat_sparse_open,
	.read	= uat_sparse_area_read,			/* bad */
	.write	= uat_sparse_area_write,		/* bad */
	.llseek	= noop_llseek,
};

/* test of casting an api to a function that isn't sparse or functions which
 * have mis-matching address spaces
 */

struct ts {
	const char *data;
	char **data2;
	const char **data3;
};

static ssize_t uat_sparse_area2_read(struct file *f, char __user *data,
				     size_t sz, loff_t *off)
{
	struct ts ts;

	memset(data, 0, sz);		/* bad */
	uat_do_something(data, sz);	/* bad */
	data[0] = '\0';			/* bad */

	ts.data = data;			/* bad */
	ts.data2 = &data;		/* bad */

	uat_fn1(data, sz);		/* bad */

	return sz;
}

static ssize_t uat_sparse_area2_write(struct file *f, const char __user *data,
				      size_t sz, loff_t *off)
{
	struct ts ts;
	char tmp[sz];		/* bad */

	printk(KERN_INFO "%s: passed %s\n", __func__, data);	/* bad */
	printk(KERN_INFO "%s: de-ref %d\n", __func__, data[0]); /* bad */

	memcpy(tmp, data, sz);		/* bad */
	uat_do_something(data, sz);	/* bad */

	ts.data = data;			/* bad */
	ts.data3 = &data;		/* bad */

	uat_fn1(data, sz);		/* bad */

	return sz;
}

static const struct file_operations uat_sparse2_fops = {
	.owner	= THIS_MODULE,
	.open	= uat_sparse_open,
	.read	= uat_sparse_area2_read,
	.write	= uat_sparse_area2_write,
	.llseek	= noop_llseek,
};

static int __init uat_sparse_init(void)
{
	debug_root = debugfs_create_dir("uat-sparse", NULL);

	debugfs_create_file("test", 0666, debug_root, NULL, &uat_sparse_fops);
	debugfs_create_file("test2", 0666, debug_root, NULL, &uat_sparse2_fops);

	pr_info("%s: initialised\n", __func__);
	return 0;
}

static void __exit uat_sparse_exit(void)
{
	debugfs_remove_recursive(debug_root);
}

module_init(uat_sparse_init);
module_exit(uat_sparse_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ben Dooks <ben.dooks@codethink.co.uk>");
MODULE_DESCRIPTION("Helper code for testing user<>kernel code");

