/* arm-user.S
 *
 * Ben Dooks <ben.dooks@codethink.co.uk>
 * Copyright 2018 (c) Codethink Ltd.
 */

#include <linux/linkage.h>
#include <asm/assembler.h>
#include <asm/errno.h>

	.text
ENTRY(__test_getuser_4)
1:	ldr	r2, [ r0 ]
	str	r2, [ r1 ]
	mov	r0, #0		/* return 0 if we success */
	ret	lr
ENDPROC(__test_getuser_4)

ENTRY(__test_putuser_4)
2:	str	r1, [ r0 ]
	mov	r0, #0		/* return 0 if we success */
	ret	lr
ENDPROC(__test_putuser_4)

__test_exception:		/* taken if the ldr/str fails */
        mov     r2, #0
        mov     r0, #-EIO	/* return -EIO to the caller */
        ret     lr		/* and return */

.pushsection __ex_table, "a"
	.long	1b, __test_exception
	.long	2b, __test_exception
.popsection
