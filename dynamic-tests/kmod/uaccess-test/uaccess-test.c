/* uaccess-test.c
 *
 * Ben Dooks <ben.dooks@codethink.co.uk>
 * Copyright 2018 (c) Codethink Ltd.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/debugfs.h>
#include <linux/cdev.h>
#include <linux/slab.h>

#define DEV_NAME "uat"
#define CLASS_NAME "uat"

#define MAX_DEVS 64
#define TEST_MAX_BUFFER		(8192)

static int uat_major;
static int uat_minor;
static dev_t uat_dev;
static struct class *uat_class;
static struct dentry *debug_root;

struct uat_test {
	dev_t		dev;
	struct device	*device;
	struct cdev	cdev;
	struct dentry	*debugfs;
	const char	*name;
	int		alloc;

	struct file_operations fops;
	ssize_t (*read) (struct file *, char __user *, size_t, loff_t *);
	ssize_t (*write) (struct file *, const char __user *, size_t, loff_t *);
};

/* tracking state and any per-session data.
 *
 * We keep our test buffer as allocating more than 1KiB of data on the
 * stack is frowned on (and if we want significantly more then impossible)
 * to do.
 */
struct test_session {
	struct uat_test	*test;
	int		buff_sz;
	uint8_t		buff[0];
};

/*
 note, include/linux/fs.h for struct file_operations
  ssize_t (*read) (struct file *, char __user *, size_t, loff_t *);
  ssize_t (*write) (struct file *, const char __user *, size_t, loff_t *);
*/

static ssize_t uat_area_read(struct file *f, char __user *data,
			     size_t sz, loff_t *off)
{
	struct test_session *session = f->private_data;
	int ret;

	if (sz > session->buff_sz)
		return -EMSGSIZE;

	ret = copy_to_user(data, session->buff, sz);
	if (ret)
		return -EFAULT;
	return sz;
}

static ssize_t uat_area_write(struct file *f, const char __user *data,
			      size_t sz, loff_t *off)
{
	struct test_session *session = f->private_data;
	int ret;

	if (sz > session->buff_sz)
		return -EMSGSIZE;

	ret = copy_from_user(session->buff, data, sz);
	return ret ? -EFAULT : sz;
}

/* use memcpy to copy to/from user (naughty) */

static ssize_t uat_area_dread(struct file *f, char __user *data,
			     size_t sz, loff_t *off)
{
	struct test_session *session = f->private_data;

	if (sz > session->buff_sz)
		return -EMSGSIZE;

	memcpy((__force void *)data, session->buff, sz);
	return sz;
}

static ssize_t uat_area_dwrite(struct file *f, const char __user *data,
			      size_t sz, loff_t *off)
{
	struct test_session *session = f->private_data;

	if (sz > session->buff_sz)
		return -EMSGSIZE;

	memcpy(session->buff, (__force void *)data, sz);
	return sz;
}

/* try probing into user-space. this requires a bit of assembly code that
 * can be "restarted" by the kernel's fault handler */
static int probe_user_get(u32 *result, void *ptr)
{
#if defined(CONFIG_ARCH_ARM)
	extern int __test_getuser_4(void *ptr, void *result);
	might_fault();
	return __test_getuser_4(ptr, result);
#elif defined(CONFIG_X86)
	register __inttype(*(result)) __val_gu asm("%"_ASM_DX);
	int __ret_gu;

	might_fault();

	asm volatile("call __test_getuser_4" 
		     : "=a" (__ret_gu), "=r" (__val_gu),
		     ASM_CALL_CONSTRAINT
		     : "0" (ptr));
	*result = __val_gu;
	return __ret_gu;
#else
	WARN_ON_ONCE(1);
	return -EIO;
#endif
}

static int probe_user_put(u32 value, void *ptr)
{
#if defined(CONFIG_ARCH_ARM)
	extern int __test_putuser_4(unsigned value, void *result);
	might_fault();
	return __test_getuser_4(ptr, value);
#elif defined(CONFIG_X86)
	int __ret_gu;

	might_fault();

	asm volatile("call __test_putuser_4"
		     : "=a" (__ret_gu)
		     : "0" ((unsigned)value), "c" (ptr));
	return __ret_gu;
#else
	WARN_ON_ONCE(1);
	return -EIO;
#endif
}


static int uat_probe_test(void *start, void *end)
{
	int fail = 0;
	void *ptr;

	for (ptr = start; ptr < end; ptr += 4) {
		u32 val = (u32)0;
		int err = 0;

		if (probe_user_get(&val, ptr) == 0) {
			pr_info("%s: managed to read %px, val 0x%08x\n",
				__func__, ptr, val);
			fail++;
			err++;
		}

		if (probe_user_put(0xff00ff00, ptr) == 0) {
			pr_info("%s: managed to write %px\n",
				__func__, ptr);
			fail++;
			err++;
		}

		/* print at end of each block of 2KiB for testing */
		if (((uintptr_t)ptr & 0x1fff) == 0 && err != 0 && 0)
			pr_info("%s: failed to access %px\n", __func__, ptr);
	}

	return fail;
}

/* test probing all user-space areas..
 *
 * Note, this is probably quite hacky, the vm code is copied from the
 * proc maps code.
 */
static ssize_t uat_probe_write(struct file *f, const char __user *data,
			      size_t sz, loff_t *off)
{
	struct task_struct *task;
	struct mm_struct *mm;
	struct vm_area_struct *vma, *tail_vma;
	void *start, *end;
	int fail = 0;

	task = get_current();
	pr_info("%s: task %px\n", __func__, task);
	mm = task->mm;
	pr_info("%s: mm %px\n", __func__, mm);
	if (!mm)
		return -EINVAL;

	down_read(&mm->mmap_sem);
	tail_vma = get_gate_vma(mm);

	for (vma = mm->mmap; vma != tail_vma && vma != NULL;
	     vma = vma->vm_next) {
		start = (void *)vma->vm_start;
		end = (void *)vma->vm_end;
		pr_debug("%s: test %px: %px..%px\n", __func__, vma, start, end);

		fail += uat_probe_test(start, end);
	}

	up_read(&mm->mmap_sem);
	return (fail > 0) ? -EFAULT : sz;
}

static int uat_cr4_show(struct seq_file *m, void *p)
{
#ifdef CONFIG_X86
	seq_printf(m, "0x%lux", __read_cr4());
	return 0;
#else
	return -EINVAL;
#endif
}

static int uat_cr4_open(struct inode *inode, struct file *file)
{
	return single_open(file, uat_cr4_show, inode->i_private);
}

static const struct file_operations uat_cr4_fops = {
	.owner	= THIS_MODULE,
	.open	= uat_cr4_open,
	.read	= seq_read,
	.llseek	= seq_lseek,
	.release = single_release,
};

static int uat_cr4_show_smap(struct seq_file *m, void *p)
{
#ifdef CONFIG_X86
	seq_printf(m, "%d", (__read_cr4() & X86_CR4_SMAP) ? 1 : 0);
	return 0;
#else
	return -EINVAL;
#endif
}

static int uat_cr4_smap_open(struct inode *inode, struct file *file)
{
	return single_open(file, uat_cr4_show_smap, inode->i_private);
}

static const struct file_operations uat_cr4_smap_fops = {
	.owner	= THIS_MODULE,
	.open	= uat_cr4_smap_open,
	.read	= seq_read,
	.llseek	= seq_lseek,
	.release = single_release,
};

/* common code for all the tests */

static int uat_open(struct inode *inode, struct file *filp)
{
	struct test_session *session;
	struct uat_test *test = inode->i_private;

	session = kzalloc(sizeof(*session) + test->alloc, GFP_KERNEL);
	if (!session)
		return -ENOMEM;

	session->test = test;
	session->buff_sz = test->alloc;
	filp->private_data = session;
	return 0;
}

static int uat_release(struct inode *inode, struct file *filp)
{
	struct test_session *session = filp->private_data;

	kfree(session);
	return 0;
}

/* template for construction of file-operations */
static const struct file_operations uat_fops = {
	.owner	= THIS_MODULE,
	.open	= uat_open,
	.release = uat_release,
	.llseek	= noop_llseek,
};

static int uat_test_add(struct uat_test *test)
{
	int minor = uat_minor++;
	int ret;

	test->dev = MKDEV(uat_major, minor);
	cdev_init(&test->cdev, &test->fops);

	test->fops = uat_fops;
	test->fops.read = test->read;
	test->fops.write = test->write;

	ret = cdev_add(&test->cdev, test->dev, 1);
	if (ret < 0) {
		pr_err("%s: failed cdev_add\n", __func__);
		return ret;
	}

	test->device = device_create(uat_class, NULL, test->dev,
				     test->device, "%s", test->name);
	if (IS_ERR(test->device)) {
		pr_err("%s: failed to create device\n", __func__);
		return ret;
	}

	test->debugfs = debugfs_create_file(test->name, 0666, debug_root,
					    test, &test->fops);
	if (IS_ERR(test->debugfs))
		pr_warn("%s: failed to create debugfs for %s\n",
			__func__, test->name);

	pr_info("%s: added test %s (%px) (dev %x)\n",
		__func__, test->name, test, test->dev);
	return 0;
}

static void uat_test_remove(struct uat_test *test)
{
	/* don't think we need anything here at the moment */
}

/* tests that move basic data types to/from user-space */
#define TEST_USER_PTR_READ(__size) \
	static ssize_t uat_test_ptr_##__size##_read(struct file *f, char __user *data, size_t sz, loff_t *off) \
	{ \
	__size us = 0; \
	if (sz != sizeof(us)) { return -EIO; } \
	if (put_user(us, data)) { return -EFAULT; } \
	return sz; \
}

#define TEST_USER_PTR_WRITE(__size)					\
	static ssize_t uat_test_ptr_##__size##_write(struct file *f, const char __user *data, size_t sz, loff_t *off) \
	{ \
	__size us; \
	if (sz != sizeof(us)) { return -EIO; } \
	if (get_user(us, data)) { return -EFAULT; } \
	return sz; \
}

#define TEST_USER_PTR(__size) \
	TEST_USER_PTR_READ(__size); TEST_USER_PTR_WRITE(__size) \

TEST_USER_PTR(u8);
TEST_USER_PTR(u16);
TEST_USER_PTR(u32);
TEST_USER_PTR(u64);

#define TEST_STRUCT(__size) { .name = "uat-"__stringify(__size), .read = uat_test_ptr_##__size##_read, .write = uat_test_ptr_##__size##_write, }

static struct uat_test tests[] = {
	{ .name = "uat-area", .alloc = TEST_MAX_BUFFER, .read = uat_area_read, .write = uat_area_write },
	{ .name = "uat-direct", .alloc = TEST_MAX_BUFFER, .read = uat_area_dread, .write = uat_area_dwrite },
	{ .name = "uat-probe", .read = NULL, .write = uat_probe_write, },
	TEST_STRUCT(u8),
	TEST_STRUCT(u16),
	TEST_STRUCT(u32),
	TEST_STRUCT(u64),
};

static int __init uat_init(void)
{
	int t, ret;

	uat_class = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(uat_class)) {
		pr_err("%s: failed to create class\n", __func__);
		return PTR_ERR(uat_class);
	}

	ret = alloc_chrdev_region(&uat_dev, 0, MAX_DEVS, DEV_NAME);
	if (ret < 0) {
		pr_err("%s: alloc_chrdev_region() failed\n", __func__);
		goto err1;
	}

	uat_major = MAJOR(uat_dev);

	debug_root = debugfs_create_dir("uat", NULL);
	if (IS_ERR(debug_root))
		pr_err("%s: failed to create debugfs root\n", __func__);

	debugfs_create_file("cpu_cr4", S_IRUSR | S_IRUGO, debug_root, NULL,
			    &uat_cr4_fops);
	debugfs_create_file("cpu_cr4_smap", S_IRUSR | S_IRUGO,
			    debug_root, NULL,
			    &uat_cr4_smap_fops);

	for (t = 0; t < ARRAY_SIZE(tests); t++) {
		ret = uat_test_add(tests + t);
	}

	pr_info("%s: initialised\n", __func__);

	return 0;
err1:
	class_destroy(uat_class);
	return ret;
}

static void __exit uat_exit(void)
{
	int t;

	debugfs_remove_recursive(debug_root);
	for (t = 0; t < ARRAY_SIZE(tests); t++)
		uat_test_remove(tests + t);

	class_destroy(uat_class);
	unregister_chrdev_region(MKDEV(uat_major, 0), MAX_DEVS);
}

module_init(uat_init);
module_exit(uat_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ben Dooks <ben.dooks@codethink.co.uk>");
MODULE_DESCRIPTION("Helper code for testing user<>kernel code");

